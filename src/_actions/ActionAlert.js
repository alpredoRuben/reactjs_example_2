import { ConstantAlert } from '../_constants'

export const ActionAlert = {
    success: () => {
        return {
            type: ConstantAlert.SUCCESS,
            message
        }
    },

    error: () => {
        return {
            type: ConstantAlert.ERROR,
            message
        };
    },

    clear: () => {
        return {
            type: ConstantAlert.CLEAR
        };
    }
};