import { combineReducers } from 'redux'
import { AuthReducer } from './AuthReducer'
import { RegistrationReducer } from './RegistrationReducer';
import { UserReducer } from './UserReducer'
import { AlertReducer } from './AlertReducer'

const rootReducer = combineReducers(
    AuthReducer,
    RegistrationReducer,
    UserReducer,
    AlertReducer
)

export default rootReducer;