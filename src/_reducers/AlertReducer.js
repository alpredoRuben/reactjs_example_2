import ConstantAlert from '../_constants';

export function AlertReducer(state={}, action) {
    switch (action.type) {
        case ConstantAlert.SUCCESS:
            return {
                type:'alert-success',
                message: action.message
            };
        case ConstantAlert.ERROR:
            return {
                type: 'alert-danger',
                message: action.message   
            };

        case ConstantAlert.CLEAR: 
            return {
                type: 'clearing',
                message: action.message
            };
        default:
            return state
    }
};