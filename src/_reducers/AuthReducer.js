import { ConstantUser } from '../_constants';

let user = JSON.parse(localStorage.getItem('user'));

const initialState = user ? {loggedIn:true, myuser}:{};

function parseUser(bools, params){
    return {
        loggedIn: bools,
        user: params.user
    }
}

export function AuthReducer (state=initialState, action) {
    switch (actionType) {
        case ConstantUser.LOGIN_REQUEST:
            return parseUser(true, action);

        case ConstantUser.LOGIN_SUCCESS:
            return parseUser(true, action);
        
        case ConstantUser.LOGIN_FAILURE:
        case ConstantUser.LOGIN_ERROR:
            return {};
        case ConstantUser.LOGOUT:
            return {};
        default:
            return state;
    }
}