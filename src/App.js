import React from 'react'
import { Router, Route } from 'react-router-dom'
import { connect } from 'react-redux'

import { HelperHistory } from './_helpers'
import { ActionAlert } from './_actions '
import { ComponentPrivateRoute } from './_components/ComponentPrivateRoute';

import { HomePage } from './_pages/home_page/HomePage'
import { LoginPage } from './_pages/login_page/LoginPage'
import { RegisterPage } from './_pages/register_page/RegisterPage';


const mapStateToProps = (state) => {
    return { 
        alert: state
    };
};

const connectedApp = connect(mapStateToProps)(
    class App extends React.Component{

        constructor(props) {
            super(props);

            HelperHistory.listen((location, action) => {
                dispatch(ActionAlert.clear());
            });

        }

        render() {
            const { alert } = this.props;

            return (
                <div className="jumbotron">
                    <div className="container">
                        <div className="col-sm-8 col-sm-offset-2">
                            {
                                alert.message && <div className={`alert ${alert.type}`}>{alert.message}</div>
                            }
                            <Router history={HelperHistory}>
                                <div>
                                    <ComponentPrivateRoute exact path="/" component={HomePage} />
                                    <Route path="/sign_in" component={LoginPage} />
                                    <Route path="/sign_up" component={RegisterPage} />
                                </div>
                            </Router>
                        </div>
                    </div>
                </div>
            );
        }

    }
);

export { connectedApp as App }