var path = require('path');
var HTML_Webpack_Plugin = require('html-webpack-plugin');

module.exports = {
    entry: './src/index.jsx',
    output: {
        path: path.resolve('dist'),
        filename: 'bundle.js'
    },
    resolve: {
        extensions: ['.js', '.jsx']
    },
    module: {
            loader: [
                {
                    test: /\.jsx?$/,
                    exclude:  /(node_modules|bower_components)/,
                    loader: 'babel-loader',
                    query: {
                        presets: ['react', 'es2015', 'stage-3']
                    } 
                }
            ]
    },
    plugin: [ new HTML_Webpack_Plugin({
        template: './src/index.html',
        filename: 'index.html',
        inject: 'body'
    })],
    devServer: {
        historyApiFallback: true
    },
    externals: {
        config: JSON.stringify({ apiUrl: 'http://localhost:4000'})
    }
}